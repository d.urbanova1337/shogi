package main

import (
	pxl "github.com/faiface/pixel"
	"github.com/faiface/pixel/text"

	"time"
)

// ENUMS

type Player bool

const (
	PlayerBlack Player = false
	PlayerWhite Player = true

	// Aliases so I don't have to remember the rules
	Player_UpsideDown  Player = PlayerWhite
	Player_PositionLow Player = PlayerBlack // Start pos at 123
	Player_FirstTurn   Player = PlayerBlack
)

type PieceType uint

const (
	T_King PieceType = iota // =0
	T_Rook
	T_Rook_Pr
	T_Bishop
	T_Bishop_Pr
	T_GenGold
	T_GenSilver
	T_GenSilver_Pr
	T_Knight
	T_Knight_Pr
	T_Lance
	T_Lance_Pr
	T_Pawn
	T_Pawn_Pr
	T_MaxValue // For testing the range
)

////

type SecsDT = float64
type (
	TransitionFloat struct {
		ttl, ttlStart    SecsDT
		valEnd, valStart float64

		OnFinishCallback func()
		Interp           func(t float64) float64
	}
	TransitionVec struct {
		v      pxl.Vec
		xt, yt TransitionFloat

		OnFinish func() // The ones in xt/xy are ignored.
		Interp   func(t float64) float64
	}
)

type (
	Pos  struct{ X, Y int } // Position on the board (range 1 to 9 incl.)
	PosD Pos                // Position relative to a piece's position

	Piece struct {
		TilePosition Pos
		Type         PieceType
		Loyalty      Player
		Captured     bool

		DrawPos   TransitionVec
		lastMoved time.Time
	}

	Board struct {
		// Origin of all pieces, everything else is just a pointer here
		Pieces []Piece

		Turn        int
		CurrentTurn Player
		Rotation    TransitionFloat
	}
)

type (
	Notice struct {
		Position        TransitionVec // Also keeps track of TTL of the notice
		CenteredDrawPos pxl.Vec
		text            *text.Text
	}
)
