package main

import (
	pxl "github.com/faiface/pixel"
	// "github.com/faiface/pixel/imdraw"
	gl "github.com/faiface/pixel/pixelgl"

	"math"
	"time"
)

// Global variables
var DefaultWindowResolution = pxl.Vec{X: 720, Y: 400}
var MenuVisible bool = true
var Window *gl.Window

func main() {
	gl.Run(glRun)
}

func glRun() {

	{ // Initialising Window
		var err error

		Window, err = gl.NewWindow(gl.WindowConfig{
			Title:     "Shogi | GLASSPAIN",
			Bounds:    pxl.Rect{Max: DefaultWindowResolution, Min: pxl.ZV},
			VSync:     false,
			Resizable: true,
		})
		if err != nil {
			// If we can't even make a window to run a videogame,
			// there's no point.
			Prt("Couldn't make a OpenGL window, quitting.")
			panic(err)
		}
	}

	fpsTicker := time.Tick(time.Second / 60)
	lastFrameStart := time.Now()

	InitMenu()
	Init()

	// Main game loop
	for !Window.Closed() && !Window.Pressed(KEY_QUIT) {

		dt := DurationToDt(time.Since(lastFrameStart))
		dt = math.Min(dt, 0.3)
		lastFrameStart = time.Now()

		if Window.JustPressed(KEY_PAUSE) {
			ToggleMenu()
		}
		if MenuVisible {
			// This includes the pause menu, actually
			Window.Clear(COL_NULL)
			MenuFrame(dt)
		} else {
			Update(dt)

			Window.Clear(COL_NULL)
			Draw()
		}

		Window.SwapBuffers()
		Window.UpdateInput()

		<-fpsTicker // Before looping, wait for next frame tick
	}

}
